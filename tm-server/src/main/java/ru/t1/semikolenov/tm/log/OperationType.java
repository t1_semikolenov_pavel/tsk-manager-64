package ru.t1.semikolenov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
