package ru.t1.semikolenov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.dto.request.AbstractUserRequest;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.exception.system.AccessDeniedException;
import ru.t1.semikolenov.tm.dto.model.SessionDTO;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getAuthService().validateToken(token);
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull SessionDTO session = getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}