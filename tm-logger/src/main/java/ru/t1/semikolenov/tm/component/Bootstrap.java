package ru.t1.semikolenov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.api.service.IReceiverService;
import ru.t1.semikolenov.tm.listener.EntityListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void run() {
        receiverService.receive(entityListener);
    }

}
